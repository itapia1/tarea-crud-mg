package com.it.mgdb.controller;

import com.it.mgdb.model.Persona;
import com.it.mgdb.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/controller/v1")
@RequiredArgsConstructor
public class PeresonaController {

    private final PersonaService personaService;

    @PostMapping("/save")
    public void save(@RequestBody Persona persona){
        personaService.savePersona(persona);
    }
    @GetMapping("/getFindAll")
    public List<Persona> getPersonaList(){
        return personaService.getFindAll();
    }
    @GetMapping("/getPersonaId/{idPersona}")
    public Persona getFindById(@PathVariable String idPersona){
        return personaService.finById(idPersona).get();
    }

    @DeleteMapping("/deletePerson/{id}")
    public void deleteById(@PathVariable String id){
        personaService.deletePersona(id);
    }
    @PutMapping("/updatePerson")
    public Persona updatePersona(@RequestBody Persona persona){
        return personaService.updatePersona(persona);
    }
}
