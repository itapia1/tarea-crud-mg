package com.it.mgdb.model;

import jakarta.persistence.Entity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "personas")
@Data
public class Persona {

    @Id
    private String idPersona;
    private String nombre;
    private String apellido;
    private String email;
    private Integer edad;
}
