package com.it.mgdb.service;

import com.it.mgdb.model.Persona;
import com.it.mgdb.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonaService {
    private final PersonaRepository personaRepository;

    public void savePersona(Persona persona){
        if (!personaRepository.existsById(persona.getIdPersona())){
            personaRepository.save(persona);
        }else{
            System.out.println("persona con ID: () = " + persona.getIdPersona() + " ya esta en bd");
        }
    }

    public Persona updatePersona(Persona persona){
        if (personaRepository.existsById(persona.getIdPersona())){
            personaRepository.save(persona);
            return persona;
        }else{
            System.out.println("persona = " + persona.getIdPersona()    + " NO esta en BD para actualizarla");
            return null;
        }
    }
    public List<Persona> getFindAll(){
        return personaRepository.findAll();
    }
    public Optional <Persona> finById(String idPersona){
        return personaRepository.findById(idPersona);
    }
    public void deletePersona(String idPeresona){
        personaRepository.deleteById(idPeresona);
    }


}
