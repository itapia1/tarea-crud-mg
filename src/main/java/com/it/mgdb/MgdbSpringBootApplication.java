package com.it.mgdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgdbSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MgdbSpringBootApplication.class, args);
	}

}
